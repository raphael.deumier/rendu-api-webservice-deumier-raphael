import os
import requests
import web
import json
from dotenv import load_dotenv
import xlrd
import xlsxwriter
import pandas as pd
#--------------------------------------------------------------
# from lxml.html.clean import unichr
#--------------------------------------------------------------

#Test pour les accents échoué

# u = unichr(40960) + u'abcd' + unichr(1972)
# utf8_version = u.encode('utf-8')
# type(utf8_version), utf8_version

#--------------------------------------------------------------
# Generating routes
#from main import content
#--------------------------------------------------------------

#Je voulais importer une bdd crée avec excel avec import pandas

#data_temp = pd.read_excel("bdd.xlsx")
#print(data_temp)

#--------------------------------------------------------------

#Je voulais importer une bdd crée avec excel et tester si déjà j'arrivais à avoir le nombre de feuille et le nom donnée ça a échoué
#La reponse pas de xlsx c'est xls avec l'import xlrd
document = xlrd.open_workbook("bdd.xls")
outWorkbook = xlsxwriter.Workbook("bdd.xls")

# print("Nombre de feuilles: "+str(document.nsheets))
# print("Noms des feuilles: "+str(document.sheet_names()))

#--------------------------------------------------------------
urls = (
    '/(.*)', 'listPays',

)

app = web.application(urls, globals())
load_dotenv()
APITOKEN = os.getenv('APITOKEN')
K = 273.15
class listPays:
    def GET(self, country):
        web.header("Content-Type", "application/json")
        r = requests.get("http://api.openweathermap.org/data/2.5/weather?q={city}&appid={token}".format(token=APITOKEN, city=country))

        result = {
            'Degrée Celius en temps réel = ': r.json()['main']['temp'] - K,
            'Degrée Celius minimum de la journée = ': r.json()['main']['temp_min'] - K,
            'Degrée Celius maximum de la journée = ': r.json()['main']['temp_max'] - K,
            'La météo du jour = ': r.json()['weather'][0]['main'],
            'Comment va se passer la journée = ': r.json()['weather'][0]['description'],
        }
        print("Nombre de feuilles: " + str(document.nsheets))
        outSheet = outWorkbook.get_worksheet_by_name('BDD')
        # outSheet.write("A2", "Names")
        # outSheet.write("B2", "sal")
        # print(outSheet)

        #outSheet.write(result)
        #outWorkbook.close()
        return json.dumps(result)

        # data = content.json()
        # t = data['main']['temp']
        # print("La témpérature est de {} degrés C".format(t - K))


if __name__ == "__main__":
    app.run()
